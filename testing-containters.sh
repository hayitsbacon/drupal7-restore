#!/bin/bash

set -e

cd ~
if [ ! -d "test1" ]; then
	mkdir "test1";mkdir "test2"
	touch "test1/index.html"; touch "test2/index.html"
	echo 'test1 is working' > "test1/index.html"
	echo 'test2 is working' > "test2/index.html"
fi

docker run -dt -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock jwilder/nginx-proxy
docker run -dit -p 80 -e VIRTUAL_HOST=test1.whatever -v /home/hayden/test1/:/www fnichol/uhttpd
docker run -dit -p 80 -e VIRTUAL_HOST=test2.whatever -v /home/hayden/test2/:/www fnichol/uhttpd
