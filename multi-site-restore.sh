#!/usr/bin/env bash


##
# Restore drupal site from tgz backup to a running site.
##

####### PLACE .tgz FILES IN PROJECT ROOT ########

cd ~

####################################################################################################
# Variables

PROJECT+=~/$1
# echo $PROJECT
sudo chown -R $(whoami) $PROJECT

####################################################################################################
# Run

runtime() {
        extract_db
        extract_web
	cleanup
	multi_site_copy
}

####################################################################################################
# Functions

extract_db() {
	cd $PROJECT

	if [ ! -d "./backup" ]; then
		mkdir backup
	fi 

        if [ ! -d "./db" ]; then
                mkdir db
        fi

	echo ""
	echo "Extracting database backup..."
	echo ""

	MYSQL_OUTFILE+="$PROJECT/mysql.tar"
	# echo $MYSQL_OUTFILE

        pv *mysql.t* | gunzip > "$MYSQL_OUTFILE"
        pv "$MYSQL_OUTFILE" | tar -x

        if [ -d "./backup/mysql" ]; then
                cd ./backup/mysql
	for file in *; do 
		NEWFILE="$(echo $file | sed 's/[-:.0-9]//g' | sed 's/gz$//g')"
		pv $file | gunzip > "$NEWFILE.sql"
		mv "$NEWFILE.sql" "../../db"
	done
        fi
}

extract_web() {
	cd $PROJECT
	
	if [ ! -d "./backup" ]; then
		mkdir backup
	fi 
	
	echo ""
	echo "Extracting drupal backup..."
	echo ""
	
	SITES_OUTFILE+="$PROJECT/sites.tar"
	# echo $SITES_OUTFILE

        pv *sites.t* | gunzip > "$SITES_OUTFILE"
        pv "$SITES_OUTFILE" | tar -x

        if [ -d "./var" ]; then
                cd ./var/www
                mv * ../../
        fi
}

cleanup() {
	cd $PROJECT
	echo ""
	echo "Copying backups to ./backup folder"
	echo ""
	cp -v *sites.tgz* backup
	cp -v *mysql.tgz* backup
	echo ""
	rm -rf "./backup/mysql"
	rm -f drupal*.tgz
	rm -f *.tar
	rm -rf "./var"
}	

multi_site_copy() {
	cd $PROJECT
	if [ -d "./web/" ];then 
		cd "./web/sites"
	
		SITES_IN_FOLDER=$(find . -mindepth 1 -maxdepth 1 ! -type l -printf '%f\n' | grep '.edu')
		NL_SITES_IN_FOLDER=$(echo "$SITES_IN_FOLDER" | nl)

		echo "$NL_SITES_IN_FOLDER"
		echo ""
		read -p "Restore all sites? (Y/n): " -n 1 -r
		echo ""
		
		if [[ $REPLY =~ ^[Yy]$ ]]; then
		for line in $SITES_IN_FOLDER; do
			SELECTED_SITE=$(echo $line)
			copy_site_folder
		done
		fi
	fi	
}

copy_site_folder() {
	cd $PROJECT
	echo ""
	echo "Creating folder for $SELECTED_SITE..."
	echo ""
	mkdir "./$SELECTED_SITE"	
	echo ""
	echo "Copying web folder for $SELECTED_SITE..."
	echo ""
	cp -r web "$SELECTED_SITE"
	cd "$SELECTED_SITE"
	cd "./web/sites/"
	rm -rf "./default"
	ln -s "$SELECTED_SITE" default
	echo "$SELECTED_SITE"

	fin_config_generate_multi
	init_db_multi
	fin_up_multi
	import_db_multi
}

fin_config_generate_multi() {
	cd $PROJECT

	cd $SELECTED_SITE
	fin config generate
	rm -rf docroot
	echo "$SELECTED_SITE"
	cd ".docksal" 
	echo "DOCROOT=web" >> docksal.env
}

init_db_multi() {
	cd $PROJECT
	cd $SELECTED_SITE
	cd "./web/sites/default"
	echo "$SELECTED_SITE"
	
	sed -i "s/'host' => 'localhost'/'host' => 'db'/g" settings.php
	
	SELECTED_SITE_VHOST=$(echo "$SELECTED_SITE")
	SELECTED_SITE_PMA_VHOST+="pma.$SELECTED_SITE"
	SELECTED_SITE_MYSQL_DATABASE=$(cat settings.php | grep "\s     'database'" | sed "s/[',=>]//g" | awk -v N=2 '{print $N}')
	SELECTED_SITE_MYSQL_USER=$(cat settings.php | grep "\s     'username'" | sed "s/[',=>]//g" | awk -v N=2 '{print $N}')
	SELECTED_SITE_MYSQL_PASSWORD=$(cat settings.php | grep "\s     'password'" | sed "s/[',=>]//g" | awk -v N=2 '{print $N}')
	
	cd $PROJECT
	cd $SELECTED_SITE
	cd .docksal
	echo "VIRTUAL_HOST=$SELECTED_SITE" >> docksal.env
	echo "PMA_VIRTUAL_HOST=$SELECTED_SITE_PMA_VHOST" >> docksal.env
	echo "MYSQL_DATABASE='$SELECTED_SITE_MYSQL_DATABASE'" >> docksal.env
	echo "MYSQL_USER='$SELECTED_SITE_MYSQL_USER'" >> docksal.env
	echo "MYSQL_PASSWORD='$SELECTED_SITE_MYSQL_PASSWORD'" >> docksal.env
	
	echo ""
	# read -s -p "Enter new MYSQL_ROOT_PASSWORD for $SELECTED_SITE: " SELECTED_SITE_MYSQL_ROOT_PASSWORD
	SELECTED_SITE_MYSQL_ROOT_PASSWORD=root
	echo ""
	echo "MYSQL_ROOT_PASSORD='$SELECTED_SITE_MYSQL_ROOT_PASSWORD'" >> docksal.env
	echo ""
}

fin_up_multi() {
	cd $PROJECT
	cd $SELECTED_SITE
	fin up
}

import_db_multi() {
	cd $PROJECT
	cd db
	DB_BACKUP=$(ls | grep "$SELECTED_SITE_MYSQL_DATABASE")
	
	cd $PROJECT
	cd $SELECTED_SITE
	if [ ! -d "./db" ]; then
		mkdir "./db"
	fi
	cd db
	cp "$PROJECT/db/$DB_BACKUP" . 

	echo ""
	echo "Waiting 10s for MySQL to initialize..."
	sleep 10
	fin sqli "$DB_BACKUP" --db="$SELECTED_SITE_MYSQL_DATABASE"
}

#################################################################################################FIN
time runtime
echo ""
echo "Site restore complete for $1."
