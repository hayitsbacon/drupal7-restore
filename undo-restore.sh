#!/bin/bash

set -e

cd ~ 

PROJECT+=~/$1

cd $PROJECT
fin rm 
mv backup/* .
rm -rf backup web db .docksal 
echo "Done"
