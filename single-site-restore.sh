#!/usr/bin/env bash

##
# Restore drupal site from tgz backup to a running site.
##

####### PLACE .tgz FILES IN PROJECT ROOT ########


cd ~

####################################################################################################
# Variables

PROJECT+=~/$1
# echo $PROJECT
sudo chown -R $(whoami) $PROJECT

####################################################################################################
# Run

runtime() {
	extract_db
	extract_web
	cleanup
	copy_site_folder
	symlink_original_files
	fin_config_generate 
	init_db 
	fin_up 
	import_db
	drush_clear_cache 
	drush_registry_rebuild 
}

####################################################################################################
# Functions

extract_db() {
        cd $PROJECT
	
	if [ ! -d "./backup" ]; then
		mkdir backup
	fi 

        if [ ! -d "./db" ]; then
                mkdir db
        fi

	echo ""
	echo "Extracting database backup..."
	echo ""

	MYSQL_OUTFILE+="$PROJECT/mysql.tar"
	# echo $MYSQL_OUTFILE

        pv *mysql.t* | gunzip > "$MYSQL_OUTFILE"
        pv "$MYSQL_OUTFILE" | tar -x

        if [ -d "./backup/mysql" ]; then
                cd ./backup/mysql
		for file in *; do 
			NEWFILE="$(echo $file | sed 's/[-:.0-9]//g' | sed 's/gz$//g')"
			pv $file | gunzip > "$NEWFILE.sql"
			mv "$NEWFILE.sql" "../../db"
		done		
        fi
}

extract_web() {
        cd $PROJECT

	if [ ! -d "./backup" ]; then
		mkdir backup
	fi 
	
	echo ""
	echo "Extracting drupal backup..."
	echo ""

	SITES_OUTFILE+="$PROJECT/sites.tar"

        pv *sites.t* | gunzip > "$SITES_OUTFILE"
        pv "$SITES_OUTFILE" | tar -x

        if [ -d "./var" ]; then
                cd ./var/www
                mv * ../../
        fi
}

cleanup() {
	cd $PROJECT
	echo ""
	echo "Copying backups to ./backup folder"
	echo ""
	cp -v *sites.tgz* backup
	cp -v *mysql.tgz* backup
	echo ""
	rm -rf "./backup/mysql"
	rm -f drupal*.tgz
	rm -f *.tar
	rm -rf "./var"
}

copy_site_folder() {
	cd $PROJECT
	if [ -d "./web" ];then 
		cd "./web/sites"
		SITES_IN_FOLDER=$(ls | grep '.edu')

		NL_SITES_IN_FOLDER=$(echo "$SITES_IN_FOLDER" | nl)
		
		echo ""
		echo "$NL_SITES_IN_FOLDER"
		echo ""
		read -p "Select site to restore (#): " -n 2 -r	
		echo ""

		SELECTED_SITE=$(echo "$SITES_IN_FOLDER" | awk "NR == ${REPLY}")

		rm -rf "./default"
		mv $SELECTED_SITE ./default
	fi

}

symlink_original_files() {
	cd $PROJECT
	cd web/sites
	ln -s -f default $SELECTED_SITE
}


fin_config_generate() {
	cd $PROJECT
	fin config generate
	rm -rf docroot
	cd .docksal 
	echo "DOCROOT=web" >> docksal.env
}

init_db() {
	cd $PROJECT
	cd web/sites/default

	sed -i "s/'host' => 'localhost'/'host' => 'db'/g" settings.php
	
	SELECTED_SITE_VHOST=$(echo "$SELECTED_SITE")
	SELECTED_SITE_PMA_VHOST+="pma.$SELECTED_SITE"
	SELECTED_SITE_MYSQL_DATABASE=$(cat settings.php | grep "\s     'database'" | sed "s/[',=>]//g" | awk -v N=2 '{print $N}')
	SELECTED_SITE_MYSQL_USER=$(cat settings.php | grep "\s     'username'" | sed "s/[',=>]//g" | awk -v N=2 '{print $N}')
	SELECTED_SITE_MYSQL_PASSWORD=$(cat settings.php | grep "\s     'password'" | sed "s/[',=>]//g" | awk -v N=2 '{print $N}')

	cd $PROJECT
	cd .docksal
	echo "VIRTUAL_HOST=$SELECTED_SITE" >> docksal.env
	echo "PMA_VIRTUAL_HOST=$SELECTED_SITE_PMA_VHOST" >> docksal.env
	echo "MYSQL_DATABASE='$SELECTED_SITE_MYSQL_DATABASE'" >> docksal.env
	echo "MYSQL_USER='$SELECTED_SITE_MYSQL_USER'" >> docksal.env
	echo "MYSQL_PASSWORD='$SELECTED_SITE_MYSQL_PASSWORD'" >> docksal.env
	
	echo ""
	read -s -p "Enter new MYSQL_ROOT_PASSWORD for $SELECTED_SITE: " SELECTED_SITE_MYSQL_ROOT_PASSWORD
	echo ""
	echo "MYSQL_ROOT_PASSORD='$SELECTED_SITE_MYSQL_ROOT_PASSWORD'" >> docksal.env
	echo ""
}

fin_up() {
	cd $PROJECT
	fin up
}

import_db() {
	cd $PROJECT
	cd db
	DB_BACKUP=$(ls | grep "$SELECTED_SITE_MYSQL_DATABASE")
	echo ""
	echo "Waiting 10s for MySQL to initialize..."
	sleep 10
	fin sqli "$DB_BACKUP" --db="$SELECTED_SITE_MYSQL_DATABASE"
}

drush_clear_cache() {
	cd $PROJECT
 	cd web
	fin drush cc all
}

drush_registry_rebuild() {
	cd $PROJECT
	cd web
	fin drush rr all
}

#################################################################################################FIN
time runtime
echo ""
echo "Site restore complete for $SELECTED_SITE."
